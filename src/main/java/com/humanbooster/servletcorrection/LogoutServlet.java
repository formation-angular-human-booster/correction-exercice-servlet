package com.humanbooster.servletcorrection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class LogoutServlet extends HttpServlet {

    // Accessible via un lien (Me déconnecter) on a dedans une mèthode doGet
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // On détruit la session qui n'existera donc plus on aura plus aucune valeur dedans
        request.getSession().invalidate();
        // On redirige l'utilisateur vers la page de login
        response.sendRedirect("/login");
    }
}
