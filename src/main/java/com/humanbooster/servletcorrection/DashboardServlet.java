package com.humanbooster.servletcorrection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// Servlet qui gère l'affichage de notre homepage
public class DashboardServlet extends HttpServlet {


    // Mèthode qui est appelé quand on fais une requête de type GET sur l'URL /dashboard
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Ecriture dans le body de notre objet reponse d'un menu de navigation en HTML
        // + request.getContextPath() + Permet de concaténer avec notre url le chemin de base de notre projet.
        response.getWriter().append("<html>" +
                "<head></head>" +
                "<body>" +
                "<h1>Acceuil</h1>"+
                "<ul>" +
                "<li><a href='"+ request.getContextPath()+"/dashboard'>Retour à la maison</a></li>" +
                "<li><a href='/cookie'>Administration des cookies</a></li>" +
                "<li><a href='/session'>Administration des sessions</a></li>" +
                "<li><a href='/logout'>Me déconecter !!</a></li>" +
                "</ul>" +
                "</body>" +
                "</html>");
    }
}
