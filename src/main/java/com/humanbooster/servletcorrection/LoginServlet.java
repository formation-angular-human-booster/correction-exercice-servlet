package com.humanbooster.servletcorrection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class LoginServlet extends HttpServlet {
    private List<String> errorsLogin;

    // Mèthode appelé pour chaque requête GET et POST de notre servlet
    // Elle va vérifier que l'attribut Username n'existe pas
    // Si il existe ça veut dire que l'utilisateur est connecté
    // Il n'a donc pas à se reconnecter
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getSession().getAttribute("username") != null) {
            request.getRequestDispatcher("/dashboard").forward(request, response);
        }
    }

    // La mèthode doGet affiche le formulaire de login
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
        response.getWriter().append("<html>" +
                "<head></head>" +
                "<body>" +
                "<h1>Formulaire de login</h1>"+
                "<form method='post'>" +
                "<label>Username</label>" +
                "<input type='text' placeholder='username' name='username'/> <br>" +

                "<label>Password</label>" +
                "<input type='password' placeholder='username' name='password'/> <br>" +
                "<input type='submit' value='valider'/>" +
                "</form>" +
                "</body" +
                "</html");
    }

    // Vérifie lors de la soumission du formulaire que les username et passwords sont correctes

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        // Si c'est le cas on enregistre une variable de session et on redirige l'utilisateur vers le dashboard
        if(username.equals("aurelien") && password.equals("aurelien")) {
            request.getSession().setAttribute("username", username);
            response.sendRedirect("/dashboard");
        } else {
            // Sinon on lui raffiche le formulaire pour qu'il retente sa chance
            this.doGet(request, response);
        }

    }


}
