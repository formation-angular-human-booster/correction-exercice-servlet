package com.humanbooster.servletcorrection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

public class ListSessionServlet extends HttpServlet {
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getSession().getAttribute("username") == null) {
            response.sendRedirect("/login");
        }
    }

    // Mèthode appelé lors d'une requête GET sur l'url /session (cf web.xml)
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
        response.getWriter().append("<html>" +
                "<head></head>" +
                "<body>" +
                "<h1>Formulaire d'ajout de session</h1>"+
                "<form method='post'>" +
                "<label>Clé</label>" +
                "<input type='text' placeholder='Nom de la session' name='session_key'/> <br>" +

                "<label>Valeur de la session</label>" +
                "<input type='text' placeholder='Valeur' name='session_value'/> <br>" +
                "<input type='submit' value='valider'/>" +
                "</form>");

        // Une enumération est comme une liste. La mèthode getAttributeNames permet de retourner tous les attributs d'un objet
        // getAttributesName permet de reccupérer tous les noms attributss de l'objet session
        Enumeration keys = request.getSession().getAttributeNames();

        response.getWriter().append("<ul>");

        // Je parcours tous les noms d'attributs de ma session
        while (keys.hasMoreElements()) {
            // Pour ma boucle while je vais chercher l'élément suivant à chaque occurence
            String key = (String) keys.nextElement();
            // J'affiche à mon utilisateur le nom de mon attriut puis la valeur de cet attribut
            response.getWriter().append("<li>").append(key).append(" - ")
                    .append((String) request.getSession().getAttribute(key)).append("</li>");
        }

        response.getWriter().append("</ul>");
        response.getWriter().append("</body>" +
                "</html");
    }


    // Dans le doPost il ajoute un nouvel attribut en session qui aura pour valeur le champ qui a l'attribut name session_value
    // dans notre formulaire HTML
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
        String key = request.getParameter("session_key");
        String value = request.getParameter("session_value");

       request.getSession().setAttribute(key, value);

        response.sendRedirect("/session");
    }
}
