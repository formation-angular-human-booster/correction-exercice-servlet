package com.humanbooster.servletcorrection;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// Servlet qui sera appelé quand on accedera a l'url /cookie cf : web.xml
public class ListCookieServlet extends HttpServlet {

    // Process request sera appelé dans nos mèthodes doPost et doGet. Il permettra de vérifier que l'utilisateur est bien connecté
    // Dans le cas contraire il redirigera l'utilisateur vers l'URL de login
    // Dans mon cas il retourne un boolean pour indiquer à notre mèthode doGet ou doPost si l'utilisateur est connecté
    private boolean processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Ici je réccupére la requête dans ma session Si l'attribut de session username est égal à null je redirige mon utilisateur
        if(request.getSession().getAttribute("username") == null) {
            response.sendRedirect("/login");
            return false;
        } else {
            return true;
        }

    }


    // Mèthode appelé quand on fait une requête de type GET sur l'url /cookie Elle affiche un formulaire de création de cookie
    // Elle affiche aussi la liste de cookie contenu dans mon navigateur puisque les cookies sont stockés sur le client
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
        response.getWriter().append("<html>" +
                "<head></head>" +
                "<body>" +
                "<h1>Formulaire d'ajout de cookie</h1>"+
                "<form method='post'>" +
                "<label>Clé</label>" +
                "<input type='text' placeholder='Nom du cookie' name='cookie_key'/> <br>" +

                "<label>Valeur du cookie</label>" +
                "<input type='text' placeholder='Valeur' name='cookie_value'/> <br>" +
                "<input type='submit' value='valider'/>" +
                "</form>");

        if(request.getCookies() != null) {
            // Je réccupére ici les cookies qui sont stockés dans ma requête
            Cookie[] cookies = request.getCookies();
            response.getWriter().append("<ul>");

            // Je parcours tous mes cookies pour afficher leur nom et leur valeur.
            for(Cookie cookie : cookies) {
                response.getWriter().append("<li>").append(cookie.getName())
                        .append(" - ").append(cookie.getValue()).append("</li>");
            }
            response.getWriter().append("</ul>");
        } else {
            // Si j'ai aucun cookie j'affiche un message indiquant que je n'ai rien à afficher à l'utilisateur
            response.getWriter().append("Aucun cookie installé");
        }

        response.getWriter().append("</body>" +
                "</html");
    }


    // Cette mèthode est appelé lors d'une requête post sur l'URL /cookie
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        boolean testConnection = this.processRequest(request, response);

        // Elle vérifie que l'utilisateur a bien le droit de soumettre le formulaire
        // Si c'est le cas, elle enregistrera le cookie sinon elle redirige l'utilisateur vers le login
        if(testConnection) {

            // Je réccupére les deux champs de mon formulaire via leur attribut name (qui est envoyé dans le body de la requête POST)
            String key = request.getParameter("cookie_key");
            String value = request.getParameter("cookie_value");

            // Je cré un objet Cookie de java avec ces 2 attributs
            Cookie cookie = new Cookie(key, value);

            // J'ajoute le cookie à ma requête afin que mon navigateur l'enregistre
            response.addCookie(cookie);

            // Je redirige mon utilisateur vers la liste de ses cookie
            response.sendRedirect("/cookie");
        }


    }
}
